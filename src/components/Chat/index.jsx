import styles from './styles.module.scss';
import React, { Component } from 'react';
import { Loader, Grid, Segment } from 'semantic-ui-react';
import Message from '../Message';
import EditModal from '../EditModal';
import Sender from '../Sender';
import UserDrop from '../UsersDrop';
import * as actions from './actions';
import { connect } from 'react-redux';
import { getMessages, getNewId } from './service';

class Chat extends Component {
    constructor(props){
        super(props);
        
        this.handleSend =  this.handleSend.bind(this);
        this.handleEdit =  this.handleEdit.bind(this);
        this.handleRemove =  this.handleRemove.bind(this);
        this.handleFilter =  this.handleFilter.bind(this);
        this.handleArrowUp =  this.handleArrowUp.bind(this);
    }

    handleSend(msg) {
        this.props.sendMessage(msg);
    }

    handleEdit(msg) {
        this.props.editMessage(msg.id, msg.text);
    }

    handleRemove(id) {
        this.props.deleteMessage(id);
    }

    handleFilter(id) {
        this.props.setFilter(id);
    }

    componentDidUpdate() {       
        const scrolling = document.getElementById("scroller");
        scrolling.scrollTop = scrolling.scrollHeight+1;
    }

    handleArrowUp(e) {
        if (e.code !== "ArrowUp") return;
        const myMsgs = this.props.messages.filter(m=> m.userId === this.props.currentUserId);
        if (myMsgs.length < 1) return;
        const last = myMsgs[myMsgs.length-1];
        this.props.toggleEdit(last.id, last.text);
    }

    componentDidMount() {
        document.addEventListener('keydown', this.handleArrowUp);

        getMessages().then(messages => {
                const uniqUsersId = [...new Set(messages.map(m=>m.userId))];
                const users = [];
                uniqUsersId.forEach(uniq => {
                    const message = messages.find(x => x.userId === uniq);
                    users.push({
                        key: message.userId,
                        text: message.user,
                        id: message.userId,
                        image: { 
                            avatar: true, 
                            src: message.avatar===null? "https://forwardsummit.ca/wp-content/uploads/2019/01/avatar-default.png" : message.avatar
                        }
                    });
                })
                const sorted = messages.sort((m1, m2) => (new Date(m1.createdAt)-new Date(m2.createdAt)));
                const mcount = messages.length;
                const date = new Date(sorted[mcount-1].createdAt);
                const hours = date.getHours().toString().length === 2? date.getHours().toString() : "0" + date.getHours().toString();
                const minutes = date.getMinutes().toString().length === 2? date.getMinutes().toString() : "0" + date.getMinutes().toString();
                const time = "" + hours + ":" + minutes;
                const currentUserId = getNewId();
                this.props.loadMessages(sorted, time, users, currentUserId);
                const scrolling = document.getElementById("scroller");
                scrolling.scrollTop = scrolling.scrollHeight;
            });            
      }
    

    render() {
        const data = this.props;
        var dateContainer = "";
        function isFirst(date) {
            const msgDate = new Date(date);
            const toContainer = "" + msgDate.getMonth() + msgDate.getDate();
            if (toContainer !== dateContainer) {
                dateContainer = toContainer;
                return true; 
            }
            return false;
        }
        return(
        <div>
            <div><img alt="bsa" src={require('./academy.png')}  className={styles.logo}></img></div>
            <div className={styles.content}>
                
                {!data.isLoaded? <Loader active inline="centered" /> : 
                <Grid className={styles.header} centered container columns="12">
                    <Grid.Row centered verticalAlign="middle" color="blue" >
                        <Grid.Column className="chat" width="2" textAlign="center" floated="left">
                            My chat:)
                        </Grid.Column>
                        <Grid.Column className={styles.users} width="5" textAlign="left" floated="left">
                                <Grid.Row>
                                    {data.users.length} participants
                                </Grid.Row>
                                <Grid.Row>
                                    <UserDrop handleFilter = {this.handleFilter} users = {data.users} />
                                </Grid.Row>
                        </Grid.Column>
                        <Grid.Column width="2" textAlign="left" floated="left">
                            {data.messages.length} messages
                        </Grid.Column>
                        <Grid.Column className="lastMsg" width="4" textAlign="center" floated="right">
                            Last message at {data.lastMsgDate}
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
                }
                {!data.isLoaded? "" : 
                <Segment id="scroller" raised style={ {overflow: "auto", height: "27em", background: "rgb(181, 200, 207)" }}>
                    {data.messages.map(mes=> {
                        if (data.filterId === undefined || data.filterId === null || data.filterId === mes.userId) return (
                            <Message 
                                isFirst = {isFirst(mes.createdAt)}
                                key =  {mes.id}
                                id = {mes.id}
                                user = {mes.user}
                                avatar = {mes.avatar}
                                createdAt = {mes.createdAt}
                                text = {mes.text}
                                userId = {mes.userId}
                                currentUserId = {data.currentUserId}
                                toggle = {this.props.toggleEdit}
                                remove = {this.handleRemove}
                            />);
                        return "";
                    } )
                        }
                </Segment >
                }
                {!data.isLoaded? "" : 
                <Sender userId={data.currentUserId} send={this.handleSend}/>
            }
            </div>
            {data.editableMessage && <EditModal edit = {this.handleEdit}/>}
        </div>
        ); 
    }
};

const mapStateToProps = state => {
    return {
        messages: state.messages,
        users: state.users,
        isLoaded: state.isLoaded,
        lastMsgDate: state.lastMsgDate,
        currentUserId: state.currentUserId,
        filterId: state.filterId,
        editableMessage: state.editableMessage
    }
}

const mapDispatchToProps = {...actions};

export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(Chat);
