/*fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
          .then(response => response.json())
          .then(messages => {
                const uniqUsersId = [...new Set(messages.map(m=>m.userId))];
                const users = [];
                uniqUsersId.forEach(uniq => {
                    const message = messages.find(x => x.userId === uniq);
                    users.push({
                        key: message.userId,
                        text: message.user,
                        id: message.userId,
                        image: { 
                            avatar: true, 
                            src: message.avatar===null? "https://forwardsummit.ca/wp-content/uploads/2019/01/avatar-default.png" : message.avatar
                        }
                    });
                })
                initialState.users=users;
                const mcount = messages.length;
                const time = "" + new Date(messages[mcount-1].createdAt).getHours()+':'+ new Date(messages[mcount-1].createdAt).getMinutes();
                const sorted = messages.sort((m1, m2) => (new Date(m1.createdAt)-new Date(m2.createdAt)));
                this.setState({ messages: sorted, isLoaded: true, lastMsgDate: time, users, currentUserId });
                
                initialState.messages = sorted;
                initialState.isLoaded = true;
                initialState.lastMsgDate = time;
                initialState.users=users;
                initialState.currentUserId = ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
                    (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16));
                const scrolling = document.getElementById("scroller");
                scrolling.scrollTop = scrolling.scrollHeight;
            });*/

import {LOAD_ALL_MESSAGES, SEND_MESSAGE, EDIT_MESSAGE, DELETE_MESSAGE, SET_FILTER, TOGGLE_EDIT} from './actionTypes';

export default function (state = {}, action) {
    switch (action.type) {
        case LOAD_ALL_MESSAGES: {
            return {
                ...state,
                messages: action.payload.messages,
                isLoaded: true,
                lastMsgDate: action.payload.lastMsgDate,
                users: action.payload.users,
                currentUserId: action.payload.currentUserId,
                filterId: null
            };
        }
        case SEND_MESSAGE: {
            const message = action.payload.message;
            const updated = [...state.messages, message];
            const newUsers = [...state.users];
            const date = new Date(message.createdAt);
            const hours = date.getHours().toString().length === 2? date.getHours().toString() : "0" + date.getHours().toString();
            const minutes = date.getMinutes().toString().length === 2? date.getMinutes().toString() : "0" + date.getMinutes().toString();
            const time = "" + hours + ":" + minutes;
            if (!newUsers.find(u => u.id === message.userId)) {
                newUsers.push({
                    key: message.userId,
                    text: message.user,
                    id: message.userId,
                    image: { 
                        avatar: true, 
                        src: message.avatar===null? "https://forwardsummit.ca/wp-content/uploads/2019/01/avatar-default.png" : message.avatar
                    }
                })
            }
            return {
                ...state,
                lastMsgDate: time,
                messages: updated,
                users: newUsers
            };              
        }
        case EDIT_MESSAGE: {
            const mapEdit = thisMessage => ({
                ...thisMessage,
                text: action.payload.text,
                editedAt: Date.now()
              });
            const updated = [...state.messages].map(thisMessage => (thisMessage.id !== action.payload.id 
                ? thisMessage
                : mapEdit(thisMessage)));
            return {
                ...state,
                editableMessage: undefined,
                messages: updated 
            };  
        }
        case DELETE_MESSAGE: {
            const filtered = [...state.messages].filter(m=> m.id !== action.payload.id);
            return {
                ...state,
                messages: filtered 
            };  
        }
        case SET_FILTER: {
            return {
                ...state,
                filterId: action.payload.id 
            };  
        }
        case TOGGLE_EDIT:
            return {
                ...state,
                editableMessage: action.payload.message
            };
        default:
            return state;        
    }
}
