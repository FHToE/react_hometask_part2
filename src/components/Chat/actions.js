import { LOAD_ALL_MESSAGES, SEND_MESSAGE, EDIT_MESSAGE, DELETE_MESSAGE, SET_FILTER, TOGGLE_EDIT } from './actionTypes';

export const loadMessages = (messages, lastMsgDate, users, currentUserId ) => ({
    type: LOAD_ALL_MESSAGES,
    payload: {
        messages,
        lastMsgDate,
        users,
        currentUserId
    }
});

export const sendMessage = message => ({
    type: SEND_MESSAGE,
    payload: {
        message
    }
});

export const editMessage = (id, text) => ({
    type: EDIT_MESSAGE,
    payload: {
        id,
        text
    }
});

export const deleteMessage = id => ({
    type: DELETE_MESSAGE,
    payload: {
        id
    }
});

export const setFilter = id => ({
    type: SET_FILTER,
    payload: {
        id
    }
});

export const toggleEdit = (id, text) => {
    var message;
    if (id === undefined) {
        message = undefined;
    } else {
        message =  { id, text };
    }

    return ({
    type: TOGGLE_EDIT,
    payload: {
        message
    }
})};