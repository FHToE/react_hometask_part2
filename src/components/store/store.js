import { createStore } from 'redux';
import messages from '../Chat/reducer';

export default createStore(messages);