import { Dropdown, Button, Icon } from 'semantic-ui-react';
import React, { Component, createRef } from 'react';

class UsersDrop extends Component {
    constructor(props){
        super(props);
        this.state = {
            isFiltered: false
        }        
    }




    render() {
        const users = [...this.props.users].map(u => {
            return {
                key: u.key,
                text: u.text,
                value: u.id,
                image: u.image
            }
        });
        
        const selectedRef = createRef();
        const handleFilter = () => {
            if (this.state.isFiltered) {
                this.props.handleFilter(null);
                this.setState({})
            } else {
                this.props.handleFilter(selectedRef.current.state.value);
            }
            this.setState({isFiltered: !this.state.isFiltered});
        }

        
        return (
            <div>
                <Dropdown
                    disabled = {this.state.isFiltered}
                    ref={selectedRef}
                    selection
                    search
                    placeholder="Choose user..." 
                    options={users}
                />
                <Button onClick={handleFilter} icon color={this.state.isFiltered? "olive" : "blue"}>
                    <Icon name="filter" />
                </Button>
            </div>
        );
    }

}

export default UsersDrop;