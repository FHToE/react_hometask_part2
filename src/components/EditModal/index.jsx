import { Modal, Header, Form, Button } from 'semantic-ui-react';
import React, { useState } from 'react';
import { connect } from 'react-redux';
import { toggleEdit } from '../Chat/actions';

const EditModal = ({ message, toggleEdit: toggle, edit }) => {
    const [body, setBody] = useState(message.text);
    const handleEdit = () => {
        if (body === '' || body === message.text) return;
        edit({id: message.id, text: body});
    }

    return (
        <Modal size="tiny" centered closeIcon open onClose={() => toggle()}>
            <Modal.Content>
                <Header as="h3" dividing>
                    Message
                </Header>
                <Form>
                    <Form.TextArea
                        name="body"
                        value={body}
                        onChange={ev => setBody(ev.target.value)}
                        autoFocus
                    />
                </Form>
                <Modal.Actions>
                    <Button icon="save" size="mini" color="blue" content="Save" onClick={() => handleEdit()} />
                </Modal.Actions>

            </Modal.Content>
        </Modal>
    );
}

const mapStateToProps = state => {
    return {
        message: state.editableMessage
    }
}

const mapDispatchToProps = { toggleEdit };

export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(EditModal);
