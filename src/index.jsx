import React from 'react';
import { render } from 'react-dom';
import Chat from './components/Chat';
import 'semantic-ui-css/semantic.min.css';
import './styles/common.scss';
import store from './components/store/store';
import { Provider } from 'react-redux';



const target = document.getElementById('root');
render(
    <Provider store={store}>
        <Chat />
    </Provider>
    , target);
